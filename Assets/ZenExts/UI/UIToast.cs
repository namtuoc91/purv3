﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class UIToast : MonoBehaviour
{
    public float paddingLeft;
    public float paddingRight;
	public float maximumWidth;
	public int maximumLength = 64;
    public Text icon;
    public Text message;
    public GameObject deactvie;

	public float timeAutoHide = 5.0f;
	public float defaultTimeAutoHide = 3.0f;
	public float elapsedTime;
    public ToastType toastType;
    public string defaultMessage = "";
    public UIAnimation anim;

    private int startCount;
    private RectTransform rectTransform;

    void Awake()
    {
        rectTransform = message.GetComponent<RectTransform>().parent.GetComponent<RectTransform>();
        anim = gameObject.GetComponent<UIAnimation>();
    }

    private string[] notificationMessages = new string[]
    {
        "Responsive UI",
        "Custom Animation Presets",
        "No Code Needed",
        "UIManager calculates all the resolution and aspect ratio changes and adjusts the animations accordingly. Because of this all the animations look and feel the same in both Landscape and Portrait Modes.",
        "Save your animations and load them in other projects. (simple .xml files)",
    };

    public void RandomMes()
    {
        var ran = Random.Range(0, notificationMessages.Length);
        ShowNotification(notificationMessages[ran]);
    }

    public void ShowNotification(string mes)
    {
        ShowToast(mes, ToastType.Notification, 1f);
    }

    public void ShowLoading(string mes, float _timeAutoHide = 0f)
    {
        if (_timeAutoHide == 0)
            _timeAutoHide = OldWarpChannel.Channel.recieveTimeOut;
        ShowToast(mes, ToastType.Loading, _timeAutoHide);
    }

    public void Show(string mes, ToastType _toastType = ToastType.Loading, float _timeAutoHide = 5f)
    {
        ShowToast(mes, _toastType, _timeAutoHide);
    }

	public void Hide(bool actvie = true)
    {
        if (actvie)
			deactvie.SetActive(false);

        UIManager.instance.StopCoroutine(HideNotification());
        timeAutoHide = 0f;
        UIManager.instance.StartCoroutine(HideNotification());
    }


	private void ShowToast(string _mes, ToastType _type = ToastType.Loading, float _timeAutoHide = 5f)
    {
        toastType = _type;

        if (_timeAutoHide == 0f)
            timeAutoHide = defaultTimeAutoHide;
        else
            timeAutoHide = _timeAutoHide;

        if (rectTransform == null)
            rectTransform = message.GetComponent<RectTransform>().parent.GetComponent<RectTransform>();
        if (anim == null)
            anim = gameObject.GetComponent<UIAnimation>();

        _mes = _mes.Trim();

        Debug.LogWarning("Toast Show: " + _mes + " type: " + _type);

        if (_mes.Length > maximumLength)
            _mes = _mes.Substring(0, maximumLength) + "...";

        if (!string.IsNullOrEmpty(_mes) && (message.text != _mes || anim.state == UIAnimation.State.IsHide))
        {
            message.text = _mes;

			if (maximumWidth > 0 && UIManager.uiScreenRect != null)
            {
                maximumWidth = (int)UIManager.uiScreenRect.size.x;
                var preferredWidth = message.preferredWidth;
                if (maximumWidth != 0 && preferredWidth + paddingLeft + paddingRight > maximumWidth)
                    preferredWidth = maximumWidth;
                rectTransform.sizeDelta = new Vector2(preferredWidth + paddingLeft + paddingRight, rectTransform.sizeDelta.y);
            }

            if (anim.state == UIAnimation.State.IsHide)
            {
                anim.Show();
                UIManager.instance.StartCoroutine(HideNotification());
            }
        }
        else
        {
			if (anim.state != UIAnimation.State.IsHide && !string.IsNullOrEmpty(_mes))
                message.text = _mes;
            gameObject.SetActive(toastType == ToastType.Loading);
        }

        elapsedTime = 0;
        deactvie.SetActive(toastType == ToastType.Loading);
    }

    private string ToastIcon(ToastType toast)
    {
        var icon = "";
        switch (toast)
        {
            case ToastType.Loading:
                icon = "";
                break;
            case ToastType.Notification:
                icon = "";
                break;
            case ToastType.Warning:
                icon = "";
                break;
            case ToastType.Error:
                icon = "";
                break;
        }
        return icon;
    }

    public void BackButtonEvent()
    {
		if (toastType != ToastType.Loading)
        {
            if (UIManager.instance != null)
                UIManager.instance.StopCoroutine(HideNotification());
            deactvie.SetActive(false);
			if (anim.state != UIAnimation.State.IsHide)
				anim.Hide ();
			else if (gameObject.activeSelf)
				gameObject.SetActive (false);
        }
    }

    IEnumerator HideNotification()
    {
        elapsedTime = 0f;
        while (elapsedTime < timeAutoHide)
        {
            //Debug.LogWarning("startCount : " + elapsedTime.ToString("0.0") + "/" + timeAutoHide.ToString("0.0"));
            elapsedTime += Time.deltaTime;
            yield return null;
        }

        if (anim.state != UIAnimation.State.IsHide)
            anim.Hide();
    }

    public enum ToastType
    {
        Loading,
        Notification,
        Warning,
        Error,
    }
}
