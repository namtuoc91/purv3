﻿using UnityEngine;
using System.Collections;

public class PushNotificator : MonoBehaviour {
	// use for initialization
	void Start () {
		Pushwoosh.ApplicationCode = "3AE1B-B4CB7";
		Pushwoosh.GcmProjectNumber = "4401721475";
		Pushwoosh.Instance.OnRegisteredForPushNotifications += OnRegisteredForPushNotifications;
		Pushwoosh.Instance.OnFailedToRegisteredForPushNotifications += OnFailedToRegisteredForPushNotifications;
		Pushwoosh.Instance.OnPushNotificationsReceived += OnPushNotificationsReceived;
		Pushwoosh.Instance.RegisterForPushNotifications ();
	}

	void OnRegisteredForPushNotifications(string token)
	{
		// handle here
		Debug.Log("Received token: \n" + token);
	}

	void OnFailedToRegisteredForPushNotifications(string error)
	{
		// handle here
		Debug.Log("Error ocurred while registering to push notifications: \n" + error);
	}

	void OnPushNotificationsReceived(string payload)
	{
		// handle here
		Debug.Log("Received push notificaiton: \n" + payload);

		JSONObject json = new JSONObject (payload);
		// display content of JSON
		OGUIM.MessengerBox.Show("Thông báo", json["title"].str);
	}
}