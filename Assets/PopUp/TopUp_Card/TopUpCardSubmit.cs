﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TopUpCardSubmit : MonoBehaviour
{
    public InputField seriInputField, pinInputField;
    public Text statusLabel;
    public Button buttonSubmit;
    public ScrollRect scrollRect;

    public ToggleGroup toggleCardGroup;
    public List<Toggle> toggleCards;

    private string[] listCardImage = new string[] { "viettel", "mobi", "vina", "vcoin", "zing", "vcard" };
    private string[] listCardName = new string[] { "Viettel", "Mobiphone", "Vinaphone", "Vcoin", "Zing", "Vcard" };
    private string[] listProvider = new string[] { "VT", "MOBI", "VINA", "VTC", "ZING", "VCARD" };
    private string[] listCardAvaiable = new string[] { "VT", "MOBI", "VINA", "VTC", "ZING", "VCARD" };

    public static TopUpToggleCard currentCard { get; set; }

    public void Start()
    {
        scrollRect.horizontalNormalizedPosition = 0f;
        FillCardData();

    }

    private void OnEnable()
    {
        WarpClient.wc.OnCardTopupDone += Wc_OnCardTopupDone;
    }

    private void OnDisable()
    {
        WarpClient.wc.OnCardTopupDone -= Wc_OnCardTopupDone;
    }

    public void FillCardData()
    {
        for (int i = 0; i < toggleCards.Count; i++)
        {
            try
            {
                var cardData = toggleCards[i].GetComponent<TopUpToggleCard>();
                cardData.name = listCardName[i];
                cardData.provider = listProvider[i];
				cardData.image.sprite = ImageSheet.Instance.resourcesDics["iap-card-" + listCardImage[i]];
                cardData.isAvaiable = listCardAvaiable.Any(x => x == listProvider[i]);
            }
            catch (System.Exception ex)
            {
                Debug.LogError("FillCardData: " + ex.Message);
            }
        }

        scrollRect.horizontalNormalizedPosition = 0f;
    }

    private void Wc_OnCardTopupDone(WarpResponseResultCode status)
    {
        if (status == WarpResponseResultCode.SUCCESS)
        {
            OGUIM.Toast.Show("Gửi thông tin thẻ " + currentCard.name + " thành công. Vui lòng chờ hệ thống xử lý trong giây lát.");
            seriInputField.text = "";
            pinInputField.text = "";
        }
        else
        {
            OGUIM.MessengerBox.Show("Gửi thông tin thẻ " + currentCard.name + " thất bại", "Vui lòng kiểm tra lại.");
        }
        buttonSubmit.interactable = true;
    }

    public void GetCurrentCardData()
    {
        var checkToggle = toggleCardGroup.ActiveToggles().FirstOrDefault();
        if (checkToggle != null)
        {
            currentCard = checkToggle.GetComponent<TopUpToggleCard>();
            if (currentCard != null && currentCard.isAvaiable)
            {
                statusLabel.text = "Thẻ " + currentCard.name;
                buttonSubmit.interactable = true;
            }
            else
            {
                OGUIM.Toast.ShowLoading("Hệ thống thẻ " + currentCard.name + " đang bảo trì");
                buttonSubmit.interactable = false;
            }
        }
    }

    public void SubmitCard()
    {
        GetCurrentCardData();
        if (currentCard != null)
        {
            //if (SubmitFormExtend.ValidateCard(seriInputField, pinInputField, statusLabel))
            //{
            OGUIM.Toast.ShowLoading("Đang kiểm tra giao dịch...");
            WarpRequest.CardTopup(seriInputField.text, pinInputField.text, currentCard.provider);
            //}
        }
        else
        {
            OGUIM.Toast.Show("Vui lòng chọn loại thẻ!", UIToast.ToastType.Warning);
        }
    }

    public void SetToggleCard(int indexToggle)
    {
        for (int i = 0; i < toggleCards.Count; i++)
        {
            if (i == indexToggle)
                toggleCards[i].isOn = true;
            else
                toggleCards[i].isOn = false;
        }
    }
}
