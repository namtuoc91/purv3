﻿using UnityEngine;
using System.Linq;
using UnityEngine.UI;
using System.Collections.Generic;
using DG.Tweening;

public class AchieListView : MonoBehaviour
{
    public AchieType type;
    public UIListView uiListView;
    public List<AchieData> listData = new List<AchieData>();
    public List<AchieItemView> listView = new List<AchieItemView>();
    public AchieData currentData;
    public AchieListView achieListView;

    public void Awake()
    {
        achieListView = this;
    }

    public void OnEnable()
    {
        if (WarpClient.wc != null)
        {
            WarpClient.wc.OnGetUserAchievementDone += Wc_OnGetUserAchievementDone;
            Get_Data(false);
        }
    }

    public void OnDisable()
    {
        if (WarpClient.wc != null)
            WarpClient.wc.OnGetUserAchievementDone -= Wc_OnGetUserAchievementDone;
    }

    private void Wc_OnGetUserAchievementDone(WarpResponseResultCode status, List<AchieData> data)
    {
        if (status == WarpResponseResultCode.SUCCESS && data != null)
        {
            listData = new List<AchieData>();
            foreach (var i in data)
            {
                i.achieType = type;
                if (!LobbyViewListView.listSoloData.Any(x => x.id == i.zoneId))
                    listData.Add(i);
            }
            FillData(false);
        }
        else
        {
            OGUIM.Toast.Hide();
        }
    }

    public void Get_Data(bool reload)
    {
        if (!listView.Any() || reload)
        {
            OGUIM.Toast.ShowLoading("");
            uiListView.ClearList();
            listData = new List<AchieData>();
            listView = new List<AchieItemView>();

            if (type == AchieType.ACHIEVEMENT)
                WarpRequest.GetUserAchievement();
            else if (type == AchieType.MISSION)
                WarpRequest.GetDailyMission();
        }
    }

    public void FillData(bool refill)
    {
        if (refill)
        {
            uiListView.ClearList();
            listView = new List<AchieItemView>();
        }

        if (listData != null)
        {
            int count = 0;
            foreach (var i in listData)
            {
                try
                {
                    var ui = uiListView.GetUIView<AchieItemView>(uiListView.GetDetailView());
                    if (count % 2 != 0)
                        ui.GetComponent<Image>().color = new Color32(0, 0, 0, 0);

                    if (ui.FillData(i))
                        listView.Add(ui);

                    count++;
                }
                catch (System.Exception ex)
                {
                    Debug.LogError("FillData: " + ex.Message + "\n" + ex.StackTrace);
                }
            }
            OGUIM.Toast.Hide();
        }
        else
            OGUIM.Toast.ShowNotification("Không có dữ liệu, vui lòng thử lại!");
    }

    public void CheckMesToRemove()
    {
        if (listData.Any(x => x == currentData))
        {
            var temp = listData.FirstOrDefault(x => x == currentData);
            listData.Remove(temp);
            currentData = null;
            FillData(true);
        }
    }
}
