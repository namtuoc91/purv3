﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Net.NetworkInformation;
using UnityEngine;

public class HttpRequest
{
    private static HttpRequest instance;
    public static HttpRequest Instance
    {
        get
        {
            if (instance == null)
                instance = new HttpRequest();
            return instance;
        }
    }

    private static string url_initialize = OldWarpChannel.httpRequest + "initialize";
    private static string url_forgot_password = OldWarpChannel.httpRequest + "auth/forgot_password";
    private static string url_register = OldWarpChannel.httpRequest + "auth/register";
    private static string url_version_cfg = OldWarpChannel.httpRequest + "version_cfg";
    private static string url_payment_initialize = OldWarpChannel.httpRequest + "payment/initialize";
    private static string url_payment_complete = OldWarpChannel.httpRequest + "payment/complete";
    private static string url_cfg_by_key = OldWarpChannel.httpRequest + "cfg_by_key";
    private static string url_dealer_cfg = OldWarpChannel.httpRequest + "dealer_cfg";

    #region ActionOnResponse
    public delegate void GetAPITokenDelegate(WarpResponseResultCode status, string apiToken);
    public static event GetAPITokenDelegate OnGetAPITokenDone;

    public delegate void ForgotPasswordDelegate(WarpResponseResultCode status, string data);
    public static event ForgotPasswordDelegate OnForgotPasswordDone;

    public delegate void GetVersionConfigureDelegate(WarpResponseResultCode status);
    public static event GetVersionConfigureDelegate OnGetVersionConfigureDone;

    public delegate void RegisterDelegate(WarpResponseResultCode status, UserData data);
    public static event RegisterDelegate OnRegisterDone;

    public delegate void GetDealerConfigDelegate(WarpResponseResultCode status, List<UserData> data);
    public static event GetDealerConfigDelegate OnGetDealerConfigDone;

	public delegate void CheckLocationDelegate(WarpResponseResultCode status, bool result);
	public static event CheckLocationDelegate OnCheckLocationDone;
    #endregion

    public IEnumerator GetAPIToken()
    {
        WWWForm form = new WWWForm();
        form.AddField("api_key", GameBase.api_key);
        form.AddField("api_secret", GameBase.api_secret);
        WWW www = new WWW(url_initialize, form);
        yield return www;

        try
        {
            if (!string.IsNullOrEmpty(www.error))
            {
                Debug.LogError("Error " + www.error);
                OnGetAPITokenDone(WarpResponseResultCode.BAD_REQUEST, GameBase.apiToken);
            }
            else
            {
                var data = JsonUtility.FromJson<InitializeData>(www.text);
                if (data != null && data.status == 0)
                    GameBase.apiToken = data.data;
                OnGetAPITokenDone(WarpResponseResultCode.SUCCESS, GameBase.apiToken);
            }
        }
        catch (Exception ex)
        {
            Debug.Log(ex.ToString());
			OnGetAPITokenDone(WarpResponseResultCode.BAD_REQUEST, GameBase.apiToken);
        }
    }

    public IEnumerator GetVersionConfigure()
    {
        var form = new WWWForm();
        form.AddField("os_type", GameBase.osType.ToString());
        form.AddField("version", GameBase.clientVersion);
        form.AddField("providerCode", GameBase.providerCode);

        var www = new WWW(url_version_cfg, form);
        yield return www;

        try
        {
            var root = new JSONObject(www.text);
            if (root != null && root["status"].i == 0)
            {
                var data = new JSONObject(root["data"].ToString());
                if (data != null)
                {
                    var tempData = new VersionConfigData();
                    GameBase.website = data["website"] != null ? data["website"].str : "";
                    GameBase.fbFanpage = data["fb_fanpage"] != null ? data["fb_fanpage"].str : "";
                    GameBase.emailSupport = data["email_support"] != null ? data["email_support"].str : "";

                    //public List<string> hotline;

                    tempData.hotline = new List<string>();
                    var hotlineListTemp = new JSONObject(data["hotline"].ToString());
                    for (int i = 0; i < hotlineListTemp.Count; i++)
                    {
                        try
                        {
                            var hotline = new JSONObject(hotlineListTemp[i].ToString());
                            if (hotline != null)
                                tempData.hotline.Add(hotline.str);
                        }
                        catch (Exception ex)
                        {
                            Debug.LogError("GetVersionConfigure: hotlineList: " + ex.Message);
                        }
                    }


                    //public List<string> supported_versions;
                    tempData.unsupportVersions = new List<Version>();
                    var supportedVersionsListTemp = new JSONObject(data["supported_versions"].ToString());
                    for (int i = 0; i < supportedVersionsListTemp.Count; i++)
                    {
                        try
                        {
                            var version = new JSONObject(supportedVersionsListTemp[i].ToString());
                            if (version != null)
                                tempData.unsupportVersions.Add(GameBase.StringToVersion(version.str));
                        }
                        catch (Exception ex)
                        {
                            Debug.LogError("GetVersionConfigure: supportedVersionsList: " + ex.Message);
                        }
                    }

                    //public Dictionary<string, string> provider_by_package;
                    tempData.provider_by_package = data["provider_by_package"] != null ? data["provider_by_package"].ToDictionary() : new Dictionary<string, string>();

                    //public List<DownloadUrl> download_urls;
                    tempData.download_urls = new List<DownloadUrl>();
                    var downloadUrlListTemp = new JSONObject(data["download_urls"].ToString());
                    for (int i = 0; i < downloadUrlListTemp.Count; i++)
                    {
                        try
                        {
                            var downloadUrlTemp = new JSONObject(downloadUrlListTemp[i].ToString());
                            if (downloadUrlTemp != null)
                            {
                                //public string latest_version;
                                //public string link;
                                //public string provider_code;
                                //public List<string> under_review_versions;

                                var downloadUrl = new DownloadUrl();

                                downloadUrl.latest_version = downloadUrlTemp["latest_version"] != null ? new JSONObject(downloadUrlTemp["latest_version"].ToString()).str : "";
                                downloadUrl.link = downloadUrlTemp["link"] ? new JSONObject(downloadUrlTemp["link"].ToString()).str : "";
                                downloadUrl.provider_code = downloadUrlTemp["provider_code"] != null ? new JSONObject(downloadUrlTemp["provider_code"].ToString()).str : "";

                                downloadUrl.under_review_versions = new List<Version>();
                                var reviewListTemp = new JSONObject(downloadUrlTemp["under_review_versions"].ToString());
                                if (reviewListTemp = null)
                                {
                                    for (int ii = 0; ii < reviewListTemp.Count; ii++)
                                    {
                                        try
                                        {
                                            var version = reviewListTemp[ii] != null ? GameBase.StringToVersion(reviewListTemp[ii].str) : new Version();
                                            downloadUrl.under_review_versions.Add(version);
                                        }
                                        catch (Exception ex)
                                        {
                                            Debug.LogError("GetVersionConfigure: under_review_versions: " + ex.Message);
                                        }
                                    }
                                }

                                tempData.download_urls.Add(downloadUrl);
                            }
                        }
                        catch (Exception ex)
                        {
                            Debug.LogError("GetVersionConfigure: downloadUrlTemp: " + ex.Message);
                        }
                    }


                    if (tempData.provider_by_package.ContainsKey(GameBase.appPackageName))
                    {
                        GameBase.providerCode = tempData.provider_by_package[GameBase.appPackageName];

                        if (!string.IsNullOrEmpty(GameBase.providerCode) && tempData.download_urls.Any())
                        {
                            var checkPack = tempData.download_urls.Where(x => x.provider_code == GameBase.providerCode).FirstOrDefault();
                            if (checkPack != null)
                            {
                                //Check in Review
                                if (checkPack.under_review_versions != null)
                                {
                                    if (checkPack.under_review_versions.Any(x => x.Major == GameBase.currentVersion.Major &&
                                                                    x.Minor == GameBase.currentVersion.Minor &&
                                                                    x.Build == GameBase.currentVersion.Build &&
                                                                    x.Revision == GameBase.currentVersion.Revision))
                                    {
                                        GameBase.underReview = true;
                                    }
                                    else
                                    {
                                        GameBase.underReview = false;
                                    }
                                }

                                //Check Force Update
                                if (tempData.unsupportVersions.Any(x => x <= GameBase.currentVersion))
                                {
                                    GameBase.needUpdateToPlay = false;
                                    GameBase.downloadURL = checkPack.link;
                                }
                                else
                                {
                                    GameBase.needUpdateToPlay = true;
                                }
                            }
                        }
                    }
                    GameBase.versionConfigData = tempData;
                }
            }
            if (OnGetVersionConfigureDone != null)
                OnGetVersionConfigureDone(WarpResponseResultCode.SUCCESS);
        }
        catch (Exception ex)
        {
            Debug.Log(ex.ToString());
            if (OnGetVersionConfigureDone != null)
                OnGetVersionConfigureDone(WarpResponseResultCode.BAD_REQUEST);
        }
    }

    public IEnumerator GetVersionConfigureV2()
    {
        var form = new WWWForm();
        form.AddField("os_type", GameBase.osType.ToString());
        form.AddField("version", GameBase.clientVersion);
        form.AddField("providerCode", GameBase.providerCode);

        var www = new WWW(url_version_cfg, form);
        yield return www;

        try
        {
            var root = new JSONObject(www.text);
            if (root != null && root["status"].i == 0)
            {
                var data = new JSONObject(root["data"].ToString());
                if (data != null)
                {

                    var tempData = new ConfigData();

                    GameBase.website = data["web"] != null ? data["web"].str : "";
                    GameBase.fbFanpage = data["fb"] != null ? data["fb"].str : "";
                    GameBase.emailSupport = data["email"] != null ? data["email"].str : "";

                    tempData.hotline = new List<string>();
                    var hotlineListTemp = new JSONObject(data["hotline"].ToString());
                    for (int i = 0; i < hotlineListTemp.Count; i++)
                    {
                        try
                        {
                            tempData.hotline.Add(hotlineListTemp[i].str);
                        }
                        catch (Exception ex)
                        {
                            Debug.LogError("GetVersionConfigure: hotlineListTemp: " + ex.Message);
                        }
                    }

                    if (tempData.hotline.Any())
                        GameBase.hotline = tempData.hotline.FirstOrDefault();

                    tempData.unsupportVersion = new List<Version>();
                    var unsupportVersionListTemp = new JSONObject(data["unsupportVersion"].ToString());
                    for (int i = 0; i < unsupportVersionListTemp.Count; i++)
                    {
                        try
                        {
                            tempData.unsupportVersion.Add(GameBase.StringToVersion(unsupportVersionListTemp[i].str));
                        }
                        catch (Exception ex)
                        {
                            Debug.LogError("GetVersionConfigure: unsupportVersion: " + ex.Message);
                        }
                    }

                    if (tempData.unsupportVersion.Any(x => x == GameBase.currentVersion))
                        GameBase.needUpdateToPlay = true;

                    tempData.providers = new List<Provider>();
                    var providersListTemp = new JSONObject(data["providers"].ToString());
                    for (int i = 0; i < providersListTemp.Count; i++)
                    {
                        try
                        {
                            var provider = new Provider();
                            var providerTemp = new JSONObject(providersListTemp[i].ToString());
                            if (providerTemp != null)
                            {
                                provider.providerCode = providerTemp["providerCode"] != null ? providerTemp["providerCode"].str : "";
                                provider.fb = providerTemp["fb"] != null ? providerTemp["fb"].str : "";
                                if (!string.IsNullOrEmpty(provider.fb))
                                    GameBase.fbFanpage = provider.fb;

                                var packages = new List<Package>();
                                var packagesTemp = new JSONObject(providerTemp["packages"].ToString());
                                if (packagesTemp != null)
                                {
                                    for (int j = 0; j < packagesTemp.Count; j++)
                                    {
                                        try
                                        {
                                            var package = new Package();
                                            Debug.Log(packagesTemp[j].ToString());
                                            var packTemp = new JSONObject(packagesTemp[j].ToString());
                                            if (packTemp != null)
                                            {
                                                package.appId = packTemp["appId"] != null ? packTemp["appId"].str : "";
                                                package.link = packTemp["link"] != null ? packTemp["link"].str : "";
                                                package.latestVersion = packTemp["latestVersion"] != null ? GameBase.StringToVersion(packTemp["latestVersion"].str) : new Version();
                                                package.reviewVersion = packTemp["reviewVersion"] != null ? GameBase.StringToVersion(packTemp["reviewVersion"].str) : new Version();
                                                packages.Add(package);
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            Debug.LogError("GetVersionConfigure: packages: " + ex.Message + " " + ex.StackTrace);
                                        }
                                    }
                                    provider.packages = packages;
                                }
                                tempData.providers.Add(provider);
                            }
                        }
                        catch (Exception ex)
                        {
                            Debug.LogError("GetVersionConfigure: providers: " + ex.Message);
                        }
                    }

                    var checkProvider = tempData.providers.FirstOrDefault(x => x.packages.Any(xx => xx.appId == GameBase.appPackageName));
                    if (checkProvider != null)
                    {
                        GameBase.providerCode = checkProvider.providerCode;
                        var checkPack = checkProvider.packages.FirstOrDefault(x => x.appId == GameBase.appPackageName);
                        if (checkPack != null)
                        {
                            //Check underReview
                            if (GameBase.currentVersion == checkPack.reviewVersion)
                                GameBase.underReview = true;
                            else
                                GameBase.underReview = false;

                            if (GameBase.currentVersion < checkPack.latestVersion)
                                GameBase.newVersionAvaiable = true;
                            GameBase.downloadURL = checkPack.link;
                        }
                    }
                    GameBase.versionConfigDataV2 = tempData;
                }
                if (OnGetVersionConfigureDone != null)
                    OnGetVersionConfigureDone(WarpResponseResultCode.SUCCESS);
            }
        }
        catch (Exception ex)
        {
            Debug.Log(ex.ToString());
            if (OnGetVersionConfigureDone != null)
                OnGetVersionConfigureDone(WarpResponseResultCode.BAD_REQUEST);
        }
    }

    public IEnumerator ForgotPassword(string username, int mobile)
    {
        var form = new WWWForm();
        form.AddField("username", username);
        form.AddField("mobile", mobile);

        var headers = form.headers;
        headers["Content-Type"] = "application/x-www-form-urlencoded";
        headers["token"] = GameBase.apiToken;
        var www = new WWW(url_forgot_password, form.data, headers);
        yield return www;

        try
        {
            var data = JsonUtility.FromJson<InitializeData>(www.text);
            if (data != null)
            {
                OnForgotPasswordDone((WarpResponseResultCode)data.status, data.message);
            }
            else
            {
                OnForgotPasswordDone(WarpResponseResultCode.BAD_REQUEST, "");
            }
        }
        catch (Exception ex)
        {
            Debug.Log(ex.ToString());
            OnForgotPasswordDone(WarpResponseResultCode.BAD_REQUEST, "");
        }
    }

    public IEnumerator Register(string _username, string _password, string _mobile = "")
    {
        var form = new WWWForm();
        form.AddField("username", _username);
        form.AddField("password", _password);
        form.AddField("mobile", _mobile);

        form.AddField("provider_code", GameBase.providerCode);
        form.AddField("client_version", GameBase.clientVersion);
        form.AddField("platform", GameBase.platform);
        form.AddField("model", GameBase.model);
        form.AddField("device_uuid", GameBase.device_uuid);
        form.AddField("refCode", GameBase.refCode);

        var headers = form.headers;
        headers["Content-Type"] = "application/x-www-form-urlencoded";
        headers["token"] = GameBase.apiToken;
        var www = new WWW(url_register, form.data, headers);
        yield return www;

        try
        {
            var data = JsonUtility.FromJson<InitializeData>(www.text);
            if (data != null)
            {
                OnRegisterDone((WarpResponseResultCode)data.status, new UserData { username = _username, password = _password, mobile = _mobile });
            }
            else
            {
                OnRegisterDone(WarpResponseResultCode.BAD_REQUEST, null);
            }
        }
        catch (Exception ex)
        {
            Debug.Log(ex.ToString());
            OnRegisterDone(WarpResponseResultCode.BAD_REQUEST, null);
        }
    }

    public IEnumerator GetDealerConfig()
    {
        var headers = new Dictionary<string, string>();
        headers["token"] = GameBase.apiToken;
        var www = new WWW(url_dealer_cfg, null, headers);
        yield return www;

        try
        {
            var temp = JsonUtility.FromJson<RootAgencyData>(www.text.ToString());
            if (string.IsNullOrEmpty(www.error) && !string.IsNullOrEmpty(www.text))
            {
                var listData = new List<UserData>();
                var root = new JSONObject(www.text);
                if (root["status"].i == 0)
                {
                    var data = new JSONObject(root["data"].ToString());
                    var locked = new JSONObject(data["locked"].ToString());
                    if (!locked.b)
                    {
                        var agencyList = new JSONObject(data["agency"].ToString());
                        for (int i = 0; i < agencyList.Count; i++)
                        {
                            try
                            {
                                var agency = new JSONObject(agencyList[i].ToString());
                                if (agency != null)
                                {
                                    string add = agency["add"].str;
                                    string name = agency["name"].str;
                                    string tel = agency["tel"].str;
                                    string fb = agency["fb"].str;
                                    string userId = agency["userId"].str;
                                    listData.Add(new UserData { displayName = name, mobile = tel, passport = add, faceBookId = fb, id = int.Parse(userId) });
                                }
                            }
                            catch (Exception ex)
                            {
                                Debug.Log("Wc_OnGetSMSConfig Passer: " + ex.Message);
                            }
                        }
                    }

                    if (listData.Any())
                        OnGetDealerConfigDone(WarpResponseResultCode.SUCCESS, listData);
                    else
                        OnGetDealerConfigDone(WarpResponseResultCode.BAD_REQUEST, null);
                }
                else
                    OnGetDealerConfigDone(WarpResponseResultCode.BAD_REQUEST, null);
            }
            else
            {
                OnGetDealerConfigDone(WarpResponseResultCode.BAD_REQUEST, null);
            }
        }
        catch (Exception ex)
        {
            Debug.Log(ex.ToString());
            OnGetDealerConfigDone(WarpResponseResultCode.BAD_REQUEST, null);
        }
    }

	public IEnumerator CheckLocation()
	{	
		string url = "http://ipinfo.io/geo";
		WWW www = new WWW(url);
		yield return www;

		try
		{
			var data = JsonUtility.FromJson<LocationData>(www.text);
			if (data != null)
			{
				var result = (data.country == "VN" ? true : false);
				if (result == false)
					GameBase.underReview = true;
				OnCheckLocationDone(WarpResponseResultCode.SUCCESS, result);
			}
			else
			{
				OnCheckLocationDone(WarpResponseResultCode.BAD_REQUEST, false);
			}
		}
		catch (Exception ex)
		{
			Debug.Log(ex.ToString());
			OnCheckLocationDone(WarpResponseResultCode.BAD_REQUEST, false);
		}
			
	}
}
