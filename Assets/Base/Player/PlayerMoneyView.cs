﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerMoneyView : MonoBehaviour
{
    public NumberAddEffect moneyNumber;
    public Image moneyImage;

    private UserData userData;
    public static PlayerMoneyView View;

    private void Awake()
    {
        View = this;
    }

    public void FillData(UserData _userData)
    {
        if (_userData == null || moneyNumber == null || moneyImage == null)
        {
            Debug.Log("_userData = NULL or moneyNumber = NULL or moneyImage = NULL");
            return;
        }

        userData = _userData;
        if (OGUIM.currentMoney.type == MoneyType.Gold)
        {
            moneyNumber.FillData(userData.gold);
        }
        else
        {
            moneyNumber.FillData(userData.koin);
        }
        moneyImage.sprite = OGUIM.currentMoney.image;
    }

    public void FillData(MoneyType type, long value)
    {
        if (type == MoneyType.Gold)
        {
            moneyNumber.FillData(value);
            moneyImage.sprite = GameBase.moneyGold.image;
        }
        else
        {
            moneyNumber.FillData(value);
            moneyImage.sprite = GameBase.moneyKoin.image;
        }
    }

    public void UpdateBet(long value)
    {
        if (OGUIM.currentMoney.type == MoneyType.Gold)
        {
            moneyNumber.FillData(value);
        }
        else
        {
            moneyNumber.FillData(value);
        }

        moneyImage.sprite = OGUIM.currentMoney.image;

        gameObject.SetActive(value > 0);
    }

    public void ShowTopUp()
	{
		#if UNITY_ANDROID || UNITY_IOS
		if (GameBase.underReview)
		{
			OGUIM.instance.popupIAP.Show();
			return;
		}
		#endif
        OGUIM.instance.popupTopUp.Show(0);
    }
}
