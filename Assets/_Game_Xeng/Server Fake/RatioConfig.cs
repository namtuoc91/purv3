﻿
using System.Collections;
using System.Collections.Generic;

public class ratioConfig
{
	private static ratioConfig _ratioConfig;

	//Ti le trung thuong //tổng phải = 1000, tăng cái này phải giảm cái khác và ngược lại
	//cach viet: local item = 3; local number = math.random(1000); if number < rx10 then item = 1 else number < rx50 then item = 2 else ... end ; return item
	public static int[] normalRatio 	= new int[10]{100, 690, 810, 876, 920, 953, 975, 991, 997, 1000};
	public static int[] normalMultiple = new int[10]{-1, 3, 5, 10, 15, 20, 30, 40, 50, 100};

	//  10% luck (1 - 100)   = 100
	//  59% x3   (101 - 690) = 590
	//  12% x5	 (691 - 810) = 120
	// 6,6% x10  (811 - 876) = 66
	// 4.4% x15  (877 - 920) = 44
	// 3.3% x20  (921 - 953) = 33
	// 2.2% x30	 (954 - 975) = 22
	// 1.6% x40  (976 - 991) = 16
	// 0.6% x50  (992 - 997) = 5
	// 0.3% x100 (998 - 1000) = 3

	public static float luckMore = 0.04f; //4% Vào item bất kì sẽ được luck(100% thực hiện)

	// Danh sách các ô theo tỉ lệ nhân
	public static List<int[]> normalPos()
	{
		var list = new List<int[]>();

		var childLuck = new int[]{6, 18};
		list.Add (childLuck);

		var childX3 = new int[]{2, 5, 8, 11, 14, 16, 20};
		list.Add (childX3);

		var childX5 = new int[]{1, 7, 13, 19};
		list.Add (childX5);

		var childX10 = new int[]{9, 21};
		list.Add (childX10);

		var childX15 = new int[]{3, 15};
		list.Add (childX15);

		var childX20 = new int[]{4, 10, 22};
		list.Add (childX20);

		var childX30 = new int[]{16};
		list.Add (childX30);

		var childX40 = new int[]{12};
		list.Add (childX40);

		var childX50 = new int[]{23};
		list.Add (childX50);

		var childX100 = new int[]{24};
		list.Add (childX100);

		//		{6, 18}, 					   // Luck (6 small luck - 18 big luck)
		//		{2, 5, 8, 11, 14, 16, 20},     // x3 
		//		{1, 7, 13, 19}, 			   // x5  Apple
		//		{9, 21}, 					   // x10 Orange
		//		{3, 15}, 					   // x15 Mango
		//		{4, 10, 22}, 				   // x20 Bell - Watermelon
		//		{16}, 						   // x30 Star
		//		{12}, 						   // x40 77 
		//		{23}, 						   // x50 Bar50
		//		{24}, 						   // x100 Bar100
		return list;
	}

	//1. quyet dinh có thực hiện Luck hay ko
	public static float luckFalse = 0.3f; // 30% vao luck bị xịt

	//2. quyết định số lượng item khi được luck
	public static int[] numberBigLuck 	= new int[3]{4,5,6};
	public static int[] ratioBigLuck 	= new int[3]{60,90,100};
	//  60% x4    
	//  30% x5   
	//  10% x6	 

	public static int[] numberSmallLuck 	= new int[3]{1,2,3};
	public static int[] ratioSmallLuck 	= new int[3]{60,90,100};
	//  50% x1    
	//  30% x2   
	//  20% x3	 

	// 3. Bắt đầu chọn item trong các ô còn ldại (ko luck) = math.random(101, 1000)

	//Khi thắng được phép đặt cược để gấp đôi phần thưởng
	public static float winSpin = 0.4f; // 40% thắng 60% thua
}
