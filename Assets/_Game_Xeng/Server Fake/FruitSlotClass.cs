﻿using System;
using System.Collections;
using System.Collections.Generic;

public class betFruitSlot
{
	public int[] bet; 

}
// Example
// bet = new int[] { 1, 2, 3, 4, 5, 6, 7, 8 }; ~ 1 Apple 2 Orange 3 Mango 4 Bell 5 Watermelon 6 Star 7 77 8 Bar

public class resultFruitSlot
{
    public int bet;
	public int winPos;
	public bool isLuck;
	public bool isLuckFalse;
	public int numberOfLuck;
	public int[] luckPosition;
	public int winChips;
}

public class fruitSlotSetting {
	public static Dictionary<int, string> posDic = new Dictionary<int, string> () {
		{ 1,"APPLE" },
		{ 2,"APPLE" },
		{ 3,"MANGO" },
		{ 4,"WATERMELON" },
		{ 5,"WATERMELON" },
		{ 6,"LUCK" },
		{ 7,"APPLE" },
		{ 8,"ORANGE" },
		{ 9,"ORANGE" },
		{ 10,"BELL" },
		{ 11,"77" },
		{ 12,"77" },
		{ 13,"APPLE" },
		{ 14,"MANGO" },
		{ 15,"MANGO" },
		{ 16,"STAR" },
		{ 17,"STAR" },
		{ 18,"LUCK" },
		{ 19,"APPLE" },
		{ 20,"BELL" },
		{ 21,"ORANGE" },
		{ 22,"BELL" },
		{ 23,"BAR" },
		{ 24,"BAR" },
	};
}