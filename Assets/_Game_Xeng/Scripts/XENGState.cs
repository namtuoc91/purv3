﻿using System;
using System.Collections.Generic;
using System.Linq;

public abstract class XENGState
{
    public string name;
    public float timeDelay1, timeDelay2, cooldownTime;
    public abstract void Enter(XENGGameManager entity);
    public abstract void Execute(XENGGameManager entity, float deltaTime);
    public abstract void Exit(XENGGameManager entity);
}
