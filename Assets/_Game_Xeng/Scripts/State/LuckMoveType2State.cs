﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
public class LuckMoveType2State : XENGState
{
    int currentIndex;
    int targetIndex;
    float cooldownTime1;
    bool isPlaySound;
    public override void Enter(XENGGameManager entity)
    {
        if (entity.numberLightVisible < entity.result.Data.luckPosition.Length)
        {
            entity.numberLightVisible++;
        }
        else
        {
            entity.TurnLight(currentIndex, false);
            entity.fsm.ChangeState(entity.luckEndState);
            return;
        }

        currentIndex = entity.GetCurrentIndex();
        isPlaySound = false;


        if (entity.numberLightVisible == 1)
        {
            targetIndex = entity.result.Data.luckPosition[0] - 1;
        }
        else if (entity.numberLightVisible == 2)
        {
            currentIndex = entity.result.Data.luckPosition[0] - 1;
            targetIndex = entity.result.Data.luckPosition[1] - 1;
        }
        else if (entity.numberLightVisible == 3)
        {
            currentIndex = entity.result.Data.luckPosition[1] - 1;
            targetIndex = entity.result.Data.luckPosition[2] - 1;
        }
        else if (entity.numberLightVisible == 4)
        {
            currentIndex = entity.result.Data.luckPosition[2] - 1;
            targetIndex = entity.result.Data.luckPosition[3] - 1;
        }
        else if (entity.numberLightVisible == 5)
        {
            currentIndex = entity.result.Data.luckPosition[3] - 1;
            targetIndex = entity.result.Data.luckPosition[4] - 1;
        }

        timeDelay1 = 0.02f;
        timeDelay2 = 0.01f;
        cooldownTime = 0.5f;
        cooldownTime1 = 0.7f;

        entity.PlaySound("luckMoveType2", 0);
    }

    public override void Execute(XENGGameManager entity, float deltaTime)
    {
        if(cooldownTime1 > 0)
        {
            cooldownTime1 -= deltaTime;
            timeDelay2 = timeDelay2 - deltaTime;

            if(timeDelay2 <= 0)
            {
                entity.TurnLight(currentIndex, false);
                currentIndex = Random.Range(0, 23);
                entity.TurnLight(currentIndex);
                timeDelay2 = timeDelay1;
            }
        }
        else
        {
            if (!isPlaySound)
            {
                entity.PlaySound("itemLuckChoose", 1);
                isPlaySound = true;
            }
            cooldownTime -= deltaTime;
            entity.TurnLight(currentIndex, false);
            currentIndex = targetIndex;
            entity.HighLight(currentIndex, true);

            if (cooldownTime <= 0)
            {
                entity.fsm.ChangeState(entity.luckStartState);
                return;
            }
        }
    }

    public override void Exit(XENGGameManager entity)
    {
    }
}
