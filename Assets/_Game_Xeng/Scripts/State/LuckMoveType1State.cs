﻿using System;
using System.Collections.Generic;
using System.Linq;
public class LuckMoveType1State : XENGState
{
    int currentIndex;
    int targetIndex;
    bool isPlaySound;
    int dir = 1;
    public override void Enter(XENGGameManager entity)
    {
        if (entity.numberLightVisible < entity.result.Data.luckPosition.Length)
        {
            entity.numberLightVisible++;
        }
        else
        {
            entity.TurnLight(currentIndex, false);
            entity.fsm.ChangeState(entity.luckEndState);
            return;
        }

        currentIndex = entity.GetCurrentIndex();
        isPlaySound = false;
        dir = -dir;

        if (entity.numberLightVisible == 1)
        {
            targetIndex = entity.result.Data.luckPosition[0] - 1;
        }
        else if (entity.numberLightVisible == 2)
        {
            currentIndex = entity.result.Data.luckPosition[0] - 1;
            targetIndex = entity.result.Data.luckPosition[1] - 1;
        }
        else if (entity.numberLightVisible == 3)
        {
            currentIndex = entity.result.Data.luckPosition[1] - 1;
            targetIndex = entity.result.Data.luckPosition[2] - 1;
        }
        else if (entity.numberLightVisible == 4)
        {
            currentIndex = entity.result.Data.luckPosition[2] - 1;
            targetIndex = entity.result.Data.luckPosition[3] - 1;
        }
        else if (entity.numberLightVisible == 5)
        {
            currentIndex = entity.result.Data.luckPosition[3] - 1;
            targetIndex = entity.result.Data.luckPosition[4] - 1;
        }

        timeDelay1 = 0.02f;
        timeDelay2 = 0.01f;
        cooldownTime = 0.5f;

        entity.PlaySound("luckMoveType1", 0);
    }

    public override void Execute(XENGGameManager entity, float deltaTime)
    {
        if(currentIndex != targetIndex)
        {
            timeDelay2 -= deltaTime;
            if(timeDelay2 <= 0)
            {
                entity.TurnLight(currentIndex, false);

                currentIndex += dir;
                if (currentIndex < 0)
                    currentIndex = 23;
                if (currentIndex > 23)
                    currentIndex = 0;

                entity.TurnLight(currentIndex);
                timeDelay2 = timeDelay1;
            }
        }
        else
        {
            if (!isPlaySound)
            {
                entity.PlaySound("itemLuckChoose", 1);
                isPlaySound = true;
            }
            entity.HighLight(currentIndex, true);
            cooldownTime -= deltaTime;
            if(cooldownTime <= 0)
            {
                entity.fsm.ChangeState(entity.luckStartState);
                return;
            }
        }
    }

    public override void Exit(XENGGameManager entity)
    {
    }
}
