﻿using System;
using System.Collections.Generic;
using System.Linq;
public class IdleState : XENGState
{
    public override void Enter(XENGGameManager entity)
    {
        UnityEngine.Debug.Log("Go to " + name);
        entity.ResetGame();
    }

    public override void Execute(XENGGameManager entity, float deltaTime)
    {
        if (entity.bets.Any(x => x != 0))
        {
            entity.canSpin = true;
        }
        else
        {
            for (int i = 0; i < entity.prevBets.Length; i++)
                entity.bets[i] = entity.prevBets[i];

            entity.canSpin = entity.bets.Any(x => x != 0);
        }
    }

    public override void Exit(XENGGameManager entity)
    {
    }
}
