﻿using System;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
public class WinState : XENGState
{
    int number;
    public override void Enter(XENGGameManager entity)
    {
        UnityEngine.Debug.Log("Go to " + name);
        entity.goToWinState = false;
        XENGUIController.SetWinChips(entity.result.Data.winChips);

        timeDelay1 = 0.02f;
        number = 0;

        if (entity.inSpinWinEvent)
        {
            entity.inSpinWinEvent = false;
            startWinEvent(entity);
            XENGUIController.ClearAllBet();
        }
        else if (entity.inLuckWinEvent)
        {
            entity.inLuckWinEvent = false;
            var currIndex = entity.GetCurrentIndex();
            if (currIndex % 12 != 5)
            {
                entity.PlaySound(entity.itemName[currIndex], 1);
                for(int i = 0; i < entity.result.Data.luckPosition.Length; i++)
                {
                    int luckyIndex = entity.result.Data.luckPosition[i] - 1;
                    DOVirtual.DelayedCall(1f * (i + 1), () => {
                        entity.PlaySound(entity.itemName[luckyIndex], 1);
                    });
                }
                var timeDelay = (entity.result.Data.luckPosition.Length + 1) * 1f;
                DOVirtual.DelayedCall(timeDelay, () => startWinEvent(entity));
            }
            else
            {
                var luckyIndex = entity.result.Data.luckPosition[0] - 1;
                entity.PlaySound(entity.itemName[luckyIndex], 1);
                if (entity.result.Data.luckPosition.Length > 1)
                {
                    for (int i = 1; i < entity.result.Data.luckPosition.Length; i++)
                    {
                        int luck = entity.result.Data.luckPosition[i] - 1;
                        DOVirtual.DelayedCall(1f * i, () => {
                            entity.PlaySound(entity.itemName[luck], 1);
                        });
                    }
                }
                var timeDelay = 1f + (entity.result.Data.luckPosition.Length - 1) * 1f;
                DOVirtual.DelayedCall(timeDelay, () => startWinEvent(entity));
            }

            var txtBets = XENGUIController.GetTxtBets();
            if (txtBets != null)
            {
                for (int i = 0; i < txtBets.Length; i++)
                {
                    if (!entity.result.Data.luckPosition.Any(x=> entity.Equals(x, i)))
                        txtBets[i].text = "00";
                }
            }
        }
        else
        {
            var txtBets = XENGUIController.GetTxtBets();
            if(txtBets != null)
            {
                for (int i = 0; i < txtBets.Length; i++)
                {
                    if (!entity.Equals(entity.GetCurrentIndex(), i))
                        txtBets[i].text = "00";
                }
            }
            entity.PlaySound(entity.itemName[entity.GetCurrentIndex()], 1);
            DOVirtual.DelayedCall(1f, () => startWinEvent(entity));
        }
    }

    public override void Execute(XENGGameManager entity, float deltaTime)
    {
    }

    public override void Exit(XENGGameManager entity)
    {
    }

    void startWinEvent(XENGGameManager entity)
    {
        entity.inWinEvent = true;
        entity.PlaySound("win" + UnityEngine.Random.Range(1, 4), 0);

        if(entity.result.Data.winChips > entity.result.Data.bet * 5)
            XENGUIController.DoJackPot();
    }
}
