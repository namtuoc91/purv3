﻿using System;
using System.Collections.Generic;
using System.Linq;
public class ToIdleState : XENGState
{
    int step = 1;
    public override void Enter(XENGGameManager entity)
    {
        timeDelay1 = 0.02f;
        timeDelay2 = 0.012f;
        entity.PlaySound("balance_chip", 1);

        step = entity.result.Data.winChips < 20 ? 1 : (entity.result.Data.winChips / 20);
    }

    public override void Execute(XENGGameManager entity, float deltaTime)
    {
        timeDelay2 -= deltaTime;
        if(timeDelay2 <= 0)
        {
            XENGUIController.MoveWinCreditToCredits(step);
            timeDelay2 = timeDelay1;
        }
    }

    public override void Exit(XENGGameManager entity)
    {
    }
}
