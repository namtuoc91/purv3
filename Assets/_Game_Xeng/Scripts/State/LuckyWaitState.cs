﻿using System;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
public class LuckyWaitState : XENGState
{
    bool isComplete;
    public override void Enter(XENGGameManager entity)
    {
        cooldownTime = 1f;
        isComplete = false;
        entity.PlaySound("waitLuckOrNot", 0);
    }

    public override void Execute(XENGGameManager entity, float deltaTime)
    {
        cooldownTime -= deltaTime;
        if(cooldownTime <= 0 && !isComplete)
        {
            isComplete = true;
            if (entity.result.Data.isLuckFalse)
            {
                entity.PlaySound("lose", 0);
                entity.fsm.ChangeState(entity.idleState);
            }
            else
            {
                entity.fsm.ChangeState(entity.luckStartState);
            }
        }
    }

    public override void Exit(XENGGameManager entity)
    {
    }
}
