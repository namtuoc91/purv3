﻿using System;
using System.Collections.Generic;
using System.Linq;
public class LuckyEndState : XENGState
{
    public override void Enter(XENGGameManager entity)
    {
        DG.Tweening.DOVirtual.DelayedCall(1, () => {
            if (entity.goToWinState)
            {
                entity.inLuckWinEvent = true;
                entity.fsm.ChangeState(entity.winState);
            }
            else
                entity.fsm.ChangeState(entity.idleState);
        });
    }

    public override void Execute(XENGGameManager entity, float deltaTime)
    {
    }

    public override void Exit(XENGGameManager entity)
    {
    }
}
