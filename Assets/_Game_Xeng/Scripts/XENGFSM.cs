﻿using System;
using System.Collections.Generic;
using System.Linq;
public class XENGFSM
{
    public XENGGameManager entity;
    public XENGState current_state;
    public XENGState previous_state;
    public XENGState global_state;

    public void ChangeState(XENGState state)
    {
        if (state != current_state)
            previous_state = current_state;

        current_state.Exit(entity);

        current_state = state;

        current_state.Enter(entity);
    }

    public void RevertToPreviousState()
    {
        ChangeState(previous_state);
    }

    public bool IsInState(string name)
    {
        return current_state.name == name;
    }

    public bool PreInState(string name)
    {
        return previous_state.name == name;
    }

    public void Update(float deltaTime)
    {
        if (current_state != null)
            current_state.Execute(entity, deltaTime);
    }
}
