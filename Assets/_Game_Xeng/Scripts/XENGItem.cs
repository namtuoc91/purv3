﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;

public class XENGItem : MonoBehaviour {

    public Image highlight;

    Animator anim;
    Tweener fadeTweener;
    bool isHighlight;
    bool isOn;
    void Awake()
    {
        anim = GetComponent<Animator>();
        isHighlight = false;
    }
    public void SetHighlight(bool win)
    {
        anim.SetBool("isOn", win);
    }
    public void SetLight(bool enable)
    {
        if (isHighlight == enable)
            return;

        isHighlight = enable;
        if (isHighlight)
        {
            SetAlpha(1);
        }
        else
            SetAlpha(0, true);
    }

    void SetAlpha(float alpha, bool animate = false)
    {
        var color = highlight.color;
        if (animate)
        {
            if (fadeTweener != null)
            {
                fadeTweener.Kill();
                fadeTweener = null;
            }
            fadeTweener = DOVirtual.Float(color.a, alpha, 0.3f, (x) => SetAlpha(x));
        }
        else
        {
            color.a = alpha;
            highlight.color = color;
        }
    }
}
