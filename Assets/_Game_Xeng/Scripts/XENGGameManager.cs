﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using System.Collections.Generic;

public class XENGGameManager : MonoBehaviour {

    public readonly string[] itemName = { "apple", "apple", "mango", "watermelon", "watermelon", "lucky", "apple", "orange", "orange", "bell", "seven",
                                            "seven", "apple", "mango", "mango", "star", "star", "lucky", "apple", "bell", "orange", "bell", "bar", "bar"};
    public readonly string[] itemButtonName = { "apple", "orange", "mango", "bell", "watermelon", "star", "seven", "bar" };
    public XENGItem[] items;
    public AudioClip[] sounds;
    public bool canSpin, isNewGame;

    public bool inWinEvent;
    public bool inSpinWinEvent;
    public bool inLuckWinEvent;
    public int[] bets;
    public int[] prevBets;
    public XENGFSM fsm;
    public XENGState idleState, startSpin, fastSpin, endSpin, luckWaitState, luckStartState, luckEndState, luckMoveType1, luckMoveType2, winState, startNumberSpin, endNumberSpin, toIdleState;
    public bool goToWinState;
    public XengHoaQuaResult result;
    public int luckType;
    public int numberLightVisible;
    public int currentNumberRandom;
    public int originWinChip;
    public AudioSource[] audios;


    int currentIndex;
    Dictionary<string, AudioClip> soundDics;

    void Awake()
    {
        bets = new int[] { 0, 0, 0, 0, 0, 0, 0, 0 };
        prevBets = new int[] { 0, 0, 0, 0, 0, 0, 0, 0 };
        isNewGame = true;
    }
    void Start () {
        soundDics = new Dictionary<string, AudioClip>();
        foreach (var s in sounds)
            soundDics.Add(s.name, s);

        currentIndex = 0;
        luckType = 1;
        numberLightVisible = 0;
        currentNumberRandom = 0;
        inWinEvent = false;
        inLuckWinEvent = false;
        inSpinWinEvent = false;

        items[0].SetLight(true);

        idleState = new IdleState { name = "IdleState" };
        startSpin = new StartSpin { name = "StartSpin" };
        fastSpin = new FastSpin { name = "FastSpin" };
        endSpin = new EndSpin { name = "EndSpin" };
        luckWaitState = new LuckyWaitState { name = "LuckyWaitState" };
        luckStartState = new LuckyStartState { name = "LuckyStartState" };
        luckEndState = new LuckyEndState { name = "LuckyEndState" };
        luckMoveType1 = new LuckMoveType1State { name = "LuckMoveType1State" };
        luckMoveType2 = new LuckMoveType2State { name = "LuckMoveType2State" };
        winState = new WinState { name = "WinState" };
        startNumberSpin = new StartNumberSpin { name = "StartNumberSpin" };
        endNumberSpin = new EndNumberSpin { name = "EndNumberSpin" };
        toIdleState = new ToIdleState { name = "toIdleState" };

        fsm = new XENGFSM
        {
            entity = this,
            current_state = idleState
        };
        fsm.ChangeState(idleState);
    }

    void Update()
    {
        fsm.Update(Time.deltaTime);
    }

    public void ResetGame(bool all = false)
    {

        for (int i = 0; i < bets.Length; i++)
        {
            if (all)
                prevBets[i] = 0;
            else
                prevBets[i] = bets[i];
            bets[i] = 0;
        }

        numberLightVisible = 0;
        luckType = 1;
        currentNumberRandom = 0;
    }
    public void ResetLight()
    {
        for (int i = 0; i < items.Length; i++)
        {
            TurnLight(i, false);
            HighLight(i, false);
        }
        items[GetCurrentIndex()].SetLight(true);
    }
    public void SetBet(int door)
    {
        bets[door]++;
    }
    public string GetBet(int door)
    {
        return bets[door].ToString("00");
    }
    public int GetCurrentIndex()
    {
        return currentIndex % items.Length;
    }
    public int GetRangePosition(int index1, int index2)
    {
        int range = 0;

        while(index1 != index2)
        {
            index1++;
            index1 = index1 % items.Length;

            range++;
        }

        return range;
    }
    public void TurnLight(float x, bool isOn = true)
    {
        int index = Mathf.FloorToInt(x) % items.Length;

        if (isOn)
        {
            int prev = index == 0 ? 23 : (index - 1);
            items[index].SetLight(true);
            items[prev].SetLight(false);
        }
        else
        {
            items[index].SetLight(false);
        }
    }
    public void HighLight(float x, bool isOn)
    {
        int index = Mathf.FloorToInt(x) % items.Length;
        items[index].SetHighlight(isOn);
    }

    public void MoveLightPosition()
    {
        currentIndex++;
        TurnLight(currentIndex);
    }
    public void PlaySound(string name, int channel, bool loop = false)
    {
        if (!soundDics.ContainsKey(name))
            return;
        var clip = soundDics[name];
        audios[channel].Stop();
        audios[channel].clip = clip;
        audios[channel].loop = loop;
        audios[channel].Play();
    }
    public void ExitWinEvent()
    {
        if (inWinEvent)
        {
            inWinEvent = false;
            PlaySound("win", 0);
        }
    }
    public bool Equals(int indexItem, int indexButton)
    {
        var length = items.Length;
        return itemName[indexItem % length] == itemButtonName[indexButton % length];
    }
}
