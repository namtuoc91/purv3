﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using UnityEngine.UI;
using System.Linq;

public class XENGUIController : MonoBehaviour {

    public static void ClearAllBet()
    {
        if (instance == null)
        {
            Debug.LogError("XENGGameManager/ClearAllBet: instance is not exist!");
            return;
        }
        for (int i = 0; i < instance.txtBets.Length; i++)
            instance.txtBets[i].text = "00";
    }
    public static void ClearBet(int index)
    {
        if (instance == null)
        {
            Debug.LogError("XENGGameManager/ClearAllBet: instance is not exist!");
            return;
        }
        instance.txtBets[index].text = "0";
    }
    public static void ResetChips(bool ignoreWinChips = false)
    {
        if (instance == null)
        {
            Debug.LogError("XENGGameManager/SetChips: instance is not exist!");
            return;
        }

        if (!ignoreWinChips)
        {
            instance.txtWinChips.text = "0";
            if (instance.gameManager.result != null)
                USER.coin += instance.gameManager.result.Data.winChips;
        }
        instance.txtUserMoneyInGame.text = USER.coin + "";
        instance.txtUserMoneyOutGame.text = USER.coin + "";
    }
    public static void SetWinChips(int chip)
    {
        if (instance == null)
        {
            Debug.LogError("XENGGameManager/SetWinChips: instance is not exist!");
            return;
        }
        instance.txtWinChips.text = chip + "";
    }
    public static void SetTaiXiu(int taixiu)
    {
        if (instance == null)
        {
            Debug.LogError("XENGGameManager/SetTaiXiu: instance is not exist!");
            return;
        }
        instance.txtTaiXiu.text = taixiu.ToString();
    }
    public static Text[] GetTxtBets()
    {
        if (instance == null)
        {
            Debug.LogError("XENGGameManager/GetTxtBets: instance is not exist!");
            return null;
        }
        return instance.txtBets;
    }
    public static void MoveWinCreditToCredits(int step)
    {
        if (instance == null)
        {
            Debug.LogError("XENGGameManager/MoveWinCreditToCredits: instance is not exist!");
            return;
        }
        if (instance.gameManager.result.Data.winChips > 0)
        {
            instance.StackChip(true, step);
        }
        else
            instance.gameManager.fsm.ChangeState(instance.gameManager.idleState);
    }
    public static void DoJackPot()
    {
        if (instance == null)
        {
            Debug.LogError("XENGGameManager/DoJackPot: instance is not exist!");
            return;
        }
        instance.efxJackpot.Active();
    }

    public XENGGameManager gameManager;
    public Effect_Jackpot efxJackpot;
    public Text[] txtBets;
    public Text txtUserMoneyOutGame, txtUserMoneyInGame, txtWinChips, txtTaiXiu;

    public static XENGUserData USER;
    static XENGUIController instance;
    void Awake()
    {
        instance = this;
        DOTween.Init();

        LoadGame();

        ResetChips();
    }
    void OnApplicationQuit()
    {
        USER.coin += instance.gameManager.result.Data.winChips;
        SaveGame();
    }

    void LoadGame()
    {
        var data = DataController.LoadGame("XENGData");
        if (data == null)
        {
            USER = new XENGUserData();
        }
        else
            USER = data;
    }
    void SaveGame()
    {
        DataController.SaveGame(USER, "XENGData");
    }

    public void btnQuay_Clicked()
    {
        if (gameManager.fsm.IsInState("IdleState"))
        {
            if (!gameManager.canSpin)
            {
                Debug.Log("Chưa đặt cửa..!");
            }
            else
            {
                for (int i = 0; i < 8; i++)
                {
                    txtBets[i].text = gameManager.GetBet(i);
                }
                SetTaiXiu(0);
                if (!gameManager.isNewGame)
                    USER.coin -= gameManager.bets.Sum();
                ResetChips();

                var result = XengHoaQuaResult.GenarateFruitResult(gameManager.bets);
                if (result.Data != null && result.Data.luckPosition == null)
                    result.Data.luckPosition = new int[] { };
                //result.Data.luckPosition = new int[] { 7, 23, 13, 8 };
                //result.Data.winPos = 8;
                //result.Data.isLuck = false;
                //result.Data.isLuckFalse = false;
                //result.Data.winChips = 66;

                gameManager.result = result;
                gameManager.originWinChip = result.Data.winChips;
                gameManager.goToWinState = result.Data.winChips > 0;
                gameManager.luckType = Random.Range(1, 3);

                gameManager.ResetLight();
                gameManager.fsm.ChangeState(gameManager.startSpin);

            }
        }
        else if (gameManager.fsm.IsInState("WinState"))
        {
            gameManager.ExitWinEvent();
            if(gameManager.result.Data.winChips > 0)
                gameManager.fsm.ChangeState(gameManager.toIdleState);
            else
                gameManager.fsm.ChangeState(gameManager.idleState);
        }
        else if (gameManager.fsm.IsInState("ToIdleState"))
        {
            gameManager.fsm.ChangeState(gameManager.idleState);
        }
        else
        {
            Debug.Log("Vui lòng đợi..!");
        }

        //Save game
        SaveGame();
    }
    public void btnBet_Clicked(int door)
    {
        if (gameManager.fsm.IsInState("IdleState"))
        {
            if (!gameManager.isNewGame)
            {
                gameManager.isNewGame = true;
                USER.coin += gameManager.bets.Sum();
                ResetChips();
                gameManager.ResetGame(true);
                ClearAllBet();
            }
            if (door == -1)
            {
                if (USER.coin < 8)
                {
                    Debug.Log("Không đủ tiền..!");
                    return;
                }
                for (int i = 0; i < 8; i++)
                {
                    gameManager.SetBet(i);
                    txtBets[i].text = gameManager.GetBet(i);
                }
                USER.coin -= 8;
                ResetChips();
            }
            else
            {
                if(USER.coin < 1)
                {
                    Debug.Log("Không đủ tiền..!");
                    return;
                }
                gameManager.SetBet(door);
                txtBets[door].text = gameManager.GetBet(door);
                USER.coin -= 1;
                ResetChips();
            }
        }
        else if (gameManager.fsm.IsInState("WinState"))
        {
            gameManager.ExitWinEvent();
        }
        else
        {
            Debug.Log("Vui lòng đợi..!");
        }
    }
    public void btnCancel_clicked()
    {
        if (gameManager.fsm.IsInState("IdleState"))
        {
            gameManager.isNewGame = true;
            USER.coin += gameManager.bets.Sum();
            ResetChips();
            gameManager.ResetGame(true);
            ClearAllBet();
        }
        else if (gameManager.fsm.IsInState("WinState"))
        {
            gameManager.ExitWinEvent();
        }
        else
        {
            Debug.Log("Vui lòng đợi..!");
        }
    }

    public void btnTaiXiu_clicked(bool tai)
    {
        if (gameManager.fsm.IsInState("WinState"))
        {
            gameManager.ExitWinEvent();
            gameManager.currentNumberRandom = gameManager.result.spinTaiXiu(tai);

            if (gameManager.result.Data.winChips > 0)
                gameManager.goToWinState = true;

            gameManager.fsm.ChangeState(gameManager.startNumberSpin);
        }
    }

    void StackChip(bool down, int step = 1)
    {
        var offset = Mathf.Min(gameManager.result.Data.winChips, step);
        instance.gameManager.result.Data.winChips -= (down ? 1 : -1) * offset;
        USER.coin += (down ? 1 : -1) * offset;
        SetWinChips(instance.gameManager.result.Data.winChips);
        ResetChips(true);
    }
    public void btnStack_clicked(bool down)
    {
        if (down)
        {
            if(gameManager.result.Data.winChips > 0)
                StackChip(true);
        }
        else
        {
            if(gameManager.result.Data.winChips < gameManager.originWinChip)
                StackChip(false);
        }
    }
}
