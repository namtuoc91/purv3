﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AppsFlyerController : MonoBehaviour {

	// Use this for initialization
	void Start () {
        DontDestroyOnLoad(this);

#if UNITY_IOS

        AppsFlyer.setAppsFlyerKey("ZWyX2eMzxGiZDqRziqTqBn");
        AppsFlyer.setAppID("YOUR_APP_ID");

#elif UNITY_ANDROID

		AppsFlyer.init ("ZWyX2eMzxGiZDqRziqTqBn");

		AppsFlyer.setAppID (Application.bundleIdentifier); 

		// for getting the conversion data
//		AppsFlyer.loadConversionData("StartUp");

		// for in app billing validation
//		 AppsFlyer.createValidateInAppListener ("AppsFlyerTrackerCallbacks", "onInAppBillingSuccess", "onInAppBillingFailure"); 

		//For Android Uninstall
//		AppsFlyer.setGCMProjectNumber ("YOUR_GCM_PROJECT_NUMBER");

#endif

    }

    // Update is called once per frame
    void Update () {
		
	}
}
